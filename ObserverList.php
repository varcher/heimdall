<?php
/**
 * Created by PhpStorm.
* User: Vagrant Archer
* Date: 2017/7/19
* Time: 22:16
*/



$observerListOut  =   array(
	array   ("obNumber"  =>   0,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_TEST__,	    "obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "www.baidu.com",                "obName"    =>  "监视系统网络联通", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   1,	"obSysNumber"   =>  1, "obPosition" =>  __OBSERVER_POSITION_OUTTER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "www.chinalifeinvest.com.cn",	"obName"    =>  "国寿投资官网HTTP", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   3,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_OUTTER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "www.china-life-caregarden.com","obName"    =>  "国寿嘉园官网", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   4,	"obSysNumber"   =>  3, "obPosition" =>  __OBSERVER_POSITION_OUTTER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "www.chinalife.com.cn",         "obName"    =>  "中国人寿官网HTTP", "alertType" =>  __MESSENGER_TYPE_OFF__),
	array   ("obNumber"  =>   5,	"obSysNumber"   =>  4, "obPosition" =>  __OBSERVER_POSITION_OUTTER__,	"obType"    =>   __OBSERVER_TYPE_PING__,	"obAddress" =>  "www.varcher.cn",               "obName"    =>  "测试异常站点", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
);


$observerTempBatchList  =   array(
	array   ("obNumber"  =>   0,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_TEST__,	    "obType"    =>   __OBSERVER_TYPE_HTTP__, 	"obAddress" =>  "www.baidu.com",                    "obName"    =>  "公网探通", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   2,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_DNS__, 	"obAddress" =>  "mail.clic",                    "obName"    =>  "NOTES DNS解析（MAIL.CLIC）", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   3,	"obSysNumber"   =>  1, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_DNS__, 	"obAddress" =>  "clicms02.mail.clic",	        "obName"    =>  "NOTES DNS解析（clicms02.mail.clic）", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   4,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "mail.clic",                    "obName"    =>  "NOTES初始页", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   5,	"obSysNumber"   =>  4, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  'http://clicms02.mail.clic/gstz/liuwenbo.nsf/iNotes/Proxy/?OpenDocument&Form=s_ReadViewEntries&PresetFields=DBQuotaInfo;1,FolderName;($Inbox),UnreadCountInfo;1,SearchSort;DateA,hc;$98,noPI;1&TZType=UTC&Start=1&Count=17&resortdescending=5',               "obCookie" => 'Shimmer=SI_TLM:20170801T081703%2C07Z&ST_Counter:3&LAO:mail&SAB:1&CS_TLM:20170801T010143%2C68Z&DMS:17&V_TLM:20170801T022413%2C26Z; LtpaToken=AAECAzU5ODAyRTZFNTk4MDgyQ0VDTj0TwfUTzsQTsqkvT1U9E9DFE8+iE7y8E8r1E7K/L09VPUdTVFovTz1DTElD7wINzk2nk0MgmWhJGAXMkAKzQqk=; ShimmerS=ET:20170801T133158%2c00Z&R:0&AT:M&N:426F6E8846277AE63EAEDFBD72082009',  "obName"    =>  "NOTES加载文件", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   6,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "10.21.14.63",                    "obName"    =>  "NOTES初始页（IP方式）", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   7,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "http://12.3.100.16/tabid/41/Default.aspx",              "obCookie" =>'.ASPXANONYMOUS=CoVPCVcy0wEkAAAAZjg4MDNmNjItMjQxNC00Nzg2LWI1MGQtMzk0NGY5OGUzOWI50; ASP.NET_SessionId=umhvluyqtllisc45ac0hoy55; authentication=; .EASYSITE4=AE99037C0B5FC1822D1274696783DBCBEC99001EF641296F6146DC0BA33971B66F81E319E0DBF53C0AE81208A706AA70C6095E66296EE197F99E1F33FB25541CA50561A569E07008; portalaliasid=C80ADF85CD3EF32F31DA56DE282F8DFDFEE07B2C5374DDF10A75B6B92C08E9FFA47D477830B888644EB5DAEA67922FCB3E6CD2CC94ED6CCAA776E377FBA28558F97F6CA491EC659FE816602267738741; portalroles=3CC5ED0591F193C9141D1E7691111E91B01E5EB8FD2C1AD3280FA3C7BBEA41BE21EC53E599748C0AD1F8273BE9F6A3474D1AC51D0ACE3CF1B62862C1D51BEE88C95A65B5707A786692C1E4F0E0CC253E3D0D21D6153069B419DD975C33C1785B16194DBF7DF9A583EFFA2435D55F37A000394BA8E16B55A275846ABC872AC59A; language=zh-CN',             	 "obName"    =>  "内网首页", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   8,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "http://10.5.10.18/",                   		"obCookie" =>'company=zgrs; userName=liuwenbo; JSESSIONID=abcRUU6toe_gmfyQPrB2v; lastrefreshtime=20170801171226',				 "obName"    =>  "公文系统", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   9,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "http://10.5.10.17:9080/logon.jsp",                    				"obName"    =>  "绩效系统（登录页）", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   10,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "http://10.5.10.19/upsgstz/",                   				 "obName"    =>  "报销系统（登录页）", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   11,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "http://10.5.10.31:8080/aim/logon.jsp",             		       "obName"    =>  "产品管理系统（登录页）", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   12,	"obSysNumber"   =>  2, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_HTTP__,	"obAddress" =>  "http://10.5.10.26/Login.aspx",             		       "obName"    =>  "投资管理系统（登录页）", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
);

$observerNetworkList	=	array(

	array   ("obNumber"  =>   0,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.5.254",                    "obName"    =>  "广场核心(5.254)", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   1,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.8.77",                    "obName"    =>  "广场11层(77)", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   2,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.8.69",                    "obName"    =>  "广场VPN(69)", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   3,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.8.90",                    "obName"    =>  "广场出口(90)", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   4,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.8.94",                    "obName"    =>  "中心专线(94)", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   5,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.8.98",                    "obName"    =>  "中心核心(98)", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   6,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.8.106",                    "obName"    =>  "中心VPN(106)", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   7,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.15.254",                    "obName"    =>  "中心出(254)", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   8,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.226.21.29",                    "obName"    =>  "朝阳门", "alertType" =>  __MESSENGER_TYPE_WECHAT__),
	array   ("obNumber"  =>   9,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.8.110",                    "obName"    =>  "亦庄核心", "alertType" =>  __MESSENGER_TYPE_WECHAT__)
);

$observerNetworkSP	=	array(

	array   ("obNumber"  =>   4,	"obSysNumber"   =>  0, "obPosition" =>  __OBSERVER_POSITION_INNER__,	"obType"    =>   __OBSERVER_TYPE_PING__, 	"obAddress" =>  "10.5.8.94",                    "obName"    =>  "中心专线(94)", "alertType" =>  __MESSENGER_TYPE_WECHAT__));


$observerInList		=	array($observerListOut, $observerNetworkList, $observerTempBatchList, $observerNetworkSP);
