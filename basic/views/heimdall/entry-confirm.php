<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2018/1/31
 * Time: 0:29
 */
use yii\helpers\Html;
?>
<p>You have entered the following information:</p>
<ul>
    <li><label>Token</label>: <?= Html::encode($model->token) ?></li>
    <li><label>Address</label>: <?= Html::encode($model->address) ?></li>
    <li><label>Name</label>: <?= Html::encode($model->username) ?></li>
    <li><label>Password</label>: <?= Html::encode($model->password) ?></li>
</ul>