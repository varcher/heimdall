<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2018/1/31
 * Time: 0:25
 */

namespace app\models;
use Yii;
use yii\base\Model;

class HeimdallForm extends Model
{
    public $token;
    public $address;
    public $username;
    public $password;

    public function rules()
    {
        return[
        [['address','username','password'], 'required'],
        [['token'], 'string'],
            ];
    }
}