<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */


namespace app\commands;


use yii\console\Controller;
use yii;
use yii\db\Connection;
use yii\db\Command;
use yii\db\Query;
use yii\di\Instance;

date_default_timezone_set('PRC');

require_once (__DIR__."/../../constant.php");
require (__DIR__ . "/../../WechatSend.php");
require (__DIR__."/../../Classes/Universe/Universe.php");
require (__DIR__."/../../Classes/Galaxy/Galaxy.php");
require (__DIR__."/../../Classes/AfterImage/AfterImage.php");
require (__DIR__."/../../Classes/Carrier/Carrier.php");
require (__DIR__."/../../Classes/Carrier/CarrierDB.php");
require (__DIR__."/../../Classes/Arbiter/Arbiter.php");
require (__DIR__."/../../Classes/Courier/Courier.php");
require (__DIR__."/../../Classes/HistoriographerFactory/HistoriographerFactory.php");
require (__DIR__."/../../Classes/ObserverFactory/ObserverFactory.php");
require (__DIR__ . "/../../Classes/HeimdallStatic/Heimdall.php");
require (__DIR__."/../../Classes/WatchMen/WatchMen.php");
require (__DIR__."/../../Classes/Verdict/Verdict.php");
require (__DIR__."/../../Classes/Giallar/Giallar.php");
require (__DIR__."/../../Classes/Bifrost/Bifrost.php");


class HeimdallController extends Controller
{
//	public	 $db 	= 	'dbHeimdall';
	private $myPid	=	0;

	public function actionStart(int $inputUniverseNo = 0)
	{
	    $this->startProcess();

		$universe 	= 	new \Universe();
		$universe 	->	create($inputUniverseNo);
		while ($this->isContinue()) {
			$this       ->  delayProcess(__HEIMDALL_INTERVAL__);
			$universe	->	stare();				//业务处理逻辑
		}
	}


    public function actionWatchmen(int $inputHgNo = 0)
    {
        $this->startProcess();

        $watchMen = new \WatchMen();

        while ($this->isContinue()) {
            $this       ->  delayProcess(__HEIMDALL_INTERVAL__);
            $watchMen   ->  watch();
        }
    }


	public function actionStop(int $stopPid)
	{
		$this->stopProcess($stopPid);
		exit(0);
	}

    public function actionSend(string $toUser, string $subject, string $content)
    {
    	Yii::info("Sending WeChat Alert.");
		Yii::info("ToUser = {$toUser}");
		Yii::info("Subject = {$subject}");
        $weOAGiallar = new \GiallarWeOA();
        $weOAGiallar->initialize(__WECHAT_TOKEN_CORPID__, __WECHAT_TOKEN_SERECT__);
        $weOAGiallar->send($content, $subject, $toUser);
    }

    public function actionTest(string $toUser = "liuwenbo")
    {
        $zabbixConnect  =   new \BifrostWeOA();
        $weOAGiallar = new \GiallarWeOA();
        $weOAGiallar->initialize(__WECHAT_TOKEN_CORPID__, __WECHAT_TOKEN_SERECT__);
        try {
//            $zabbixConnect->zabbixStart("Admin", "zabbix", "www.varcher.cn/zabbix");
            $zabbixConnect->zabbixStart("heimdall", "heimdall", "10.5.10.128/zabbix");
            $content = $zabbixConnect->buildWeOAOverview();
            $weOAGiallar->send($content, "Heimdall Report", $toUser);
            echo "Send Message to {$toUser} successfully!";
        } catch (\Exception  $e) {
            echo $e->getMessage();
        }
    }

//	public function actionStopAll()	//todo:全部停止
//	{
//		$this->stopProcess($stopPid);
//		exit(0);
//	}

//	public function actionTestMessager(String $message)
//	{
//		sendWeChatMessage($message);
//	}

	public function init()
	{
//		$this->startProcess();
	}

	public function __destruct()
	{
		$this->endProcess();
	}

	private function startProcess()
	{
		$this->myPid	=	getmypid();
		$fileToOpen 	= fopen(__DIR_PIDFILE_PATH__."{$this->myPid}.pid", "w");
		if(fwrite($fileToOpen, 1))
		{
			echo "Process Start Successfully. PID = {$this->myPid}\n";
		}
		else
		{
			echo "START FAILED!!\n";
			exit(0);
		}
		fclose($fileToOpen);
	}

	private function endProcess()
	{
        if (0 == $this->myPid) {
            return;
        }
		if(file_exists(__DIR_PIDFILE_PATH__ . "{$this->myPid}.pid"))
		{
			unlink(__DIR_PIDFILE_PATH__ . "{$this->myPid}.pid");
		}
        echo "Process {$this->myPid} Ended.\n";

	}

	private function isContinue(): bool
	{
		return file_exists(__DIR_PIDFILE_PATH__."{$this->myPid}.pid");
	}

	private function stopProcess(int $stopPid)
	{
		if(!file_exists(__DIR_PIDFILE_PATH__ . "{$stopPid}.pid"))
		{
			echo "PID {$stopPid} not existes, probably ended already.\n";
			return;
		}
		if (unlink(__DIR_PIDFILE_PATH__ . "{$stopPid}.pid")) {
			echo "STOP PID {$stopPid} COMMAND SEND successful.";
			return;
		}
		else {
			echo "STOP PID {$stopPid} FAILED.";
			return;
		}

	}

    private function delayProcess(int $interval = 0)
    {
        static $lastTime =   0;
        $elapsedTIme     =  time() - $lastTime;
        if ($elapsedTIme < $interval) {
            sleep($interval - $elapsedTIme);
        }
        $lastTime = time();
    }
}
