<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
//namespace yii\console\controllers;

use Yii;
use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\console\Controller;


/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HiController extends Controller
{
	public $db = 'db';
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hi world')
    {
        echo $message . "\n";
    }

	public function actionMore($message)
	{
		echo "More Hi World!{$message}\n";
	}

	public function actionConn()
	{
		$this->db = Instance::ensure($this->db, Connection::className());
//		var_dump($this->db);
		$command = $this->db->createCommand("select * from observer")->queryAll();
		var_dump($command);

	}
}
