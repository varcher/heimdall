<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2018/1/31
 * Time: 0:01
 */

namespace app\controllers;

use app\models\HeimdallForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

require "../../Classes/Bifrost/Bifrost.php";

class HeimdallController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionEntry()
    {
        $model = new HeimdallForm;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $zabbixConnect  =   new \BifrostWeOA();
            $output =   "";
            try {
                $zabbixConnect->zabbixStart($model->username, $model->password, $model->address);
    //            $zabbixConnect->zabbixStart("heimdall", "heimdall", "10.5.5.43/zabbix");
                $output = $zabbixConnect->buildWeOAOverview();
            } catch (\Exception  $e) {
                $output =  $e->getMessage();
            }

            \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            return $output;

            return $this->render('entry-confirm', ['model' => $model]);
        } else {
//             无论是初始化显示还是数据验证错误
            return $this->render('entry', ['model' => $model]);
        }
    }

    public function actionWeoa_request($zabbixUrl = "www.varcher.cn/zabbix", $zabbixUsername = "Admin", $zabbixPassword = "zabbix")
    {

        $zabbixConnect  =   new \BifrostWeOA();
        echo $zabbixUrl;
        echo $zabbixUsername;
        echo $zabbixPassword;
        $output = "";
//        try {
//            $zabbixConnect->zabbixStart($zabbixUsername, $zabbixPassword, $zabbixUrl);
////            $zabbixConnect->zabbixStart("heimdall", "heimdall", "10.5.5.43/zabbix");
//            $output = $zabbixConnect->buildWeOAOverview();
//        } catch (\Exception  $e) {
//            echo $e->getMessage();
//        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'message' => $output,
            'code' => 100,
        ];
    }
}
?>