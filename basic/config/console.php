<?php

//require_once (__DIR__."/../../constant.php");

$params 		= 	require(__DIR__ . '/params.php');
$db 			= 	require(__DIR__ . '/db.php');
$db2		 	= 	require(__DIR__ . '/db2.php');
$dbHeimdall 	= 	require(__DIR__ . '/dbHeimdall.php');
$dbHeimdallLog	= 	require(__DIR__ . '/dbHeimdallLog.php');
$dbTemp 		= 	require(__DIR__ . '/dbTemp.php');
$logHeimdall 	= 	require(__DIR__ . '/logHeimdall.php');
$logDebug       =   require(__DIR__ . '/logHeimdallDebug.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
        	'flushInterval' => 1,
            'targets' => [
				$logHeimdall,
                $logDebug,
            ],
        ],
        'db' => $db,
		'db2' => $db2,
		'dbHeimdall' => $dbHeimdall,
		'dbHeimdallLog' => $dbHeimdallLog,
		'dbTemp' => $dbTemp,
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'password' => '',
            'database' => 0,
        ],
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
