<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/18
 * Time: 9:35
 */

return [
	'class' => 'yii\log\FileTarget',
	'levels' => ['error', 'warning', 'info', 'trace'],
	'logVars' => [],
	'exportInterval' => 10,
	'categories'=> ['application'],
	'logFile' => "d:/HeimdallTrace.log",
];