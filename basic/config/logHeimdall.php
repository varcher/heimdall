<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/18
 * Time: 9:35
 */

return [
	'class' => 'yii\log\FileTarget',
	'levels' => ['error', 'warning'],
	'logVars' => [],
	'exportInterval' => 1,
	'categories'=> ['application'],
	'logFile' => "d:/Heimdall.log",
];