<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/10
 * Time: 15:49
 */

class Arbiter
{

    public static function arbitrate(Verdict $verdict)
    {

        $startPointer	=	0;
        $errorRight = 0;

            //计算起始位置
        $aiList =   $verdict->getAfterImageList();
            $arraySize	=	sizeof($aiList);
            if ($arraySize < __ARBITER_COUNT_NUMBER__) {
                $startPointer	=	0;
            }
            else {
                $startPointer	=	$arraySize - __ARBITER_COUNT_NUMBER__;
            }

            //从起始位置开始计算权值
            for ($i = $startPointer; $i < $arraySize; $i++) {
                $afterImage = $aiList[$i];
                switch ($afterImage->getAiStatus()) {
                    case __AFTERIMAGE_STATUS_NORMAL__:
                        $errorRight += __ARBITER_RIGHT_NORMAL__;
                        break;
                    case __AFTERIMAGE_STATUS_FAIL__:
                        $errorRight += __ARBITER_RIGHT_FAIL__;
                        break;
                    case __AFTERIMAGE_STATUS_SLOW__:
                        $errorRight += __ARBITER_RIGHT_SLOW__;
                        break;
                    case __AFTERIMAGE_STATUS_TIMEOUT__:
                        $errorRight += __ARBITER_RIGHT_TIMEOUT__;
                        break;
                }
            }


            //权值计算完成


                if ($errorRight >= __ATBITER_RIGHT_THRESHOLD_FATAL__) {
                    $verdict->setStatus(__ARBITER_STATUS_FATAL__);
                }
                elseif ($errorRight >= __ATBITER_RIGHT_THRESHOLD_WARNING__) {
                    $verdict->setStatus(__ARBITER_STATUS_WARNING__);
                }
                else {
                    $verdict->setStatus(__ARBITER_STATUS_NORMAL__);
                }

    }

	public function loadObserverInfo()
	{

	}
}