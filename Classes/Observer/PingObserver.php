<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/8
 * Time: 13:58
 */

class PingObserver extends Observer
{
	private $targetAddress	=	"";

	public function __construct($inputObserverParams)
	{
		parent::__construct($inputObserverParams);
		$command = $this->db->createCommand("SELECT obAddress FROM observerPing WHERE observerNo =  {$this->obNumber}");
		if ($query = $command->queryOne()) {
			$this->targetAddress = $query['obAddress'];
		}
		else {
			throwException("NoSuchPingAddress");
		}
	}

	protected function doObserve(AfterImage $afterImage)
	{
		$status =   -1;
		if (strcasecmp(PHP_OS, 'WINNT') === 0) {
			// Windows 服务器下
			$pingResult = exec("ping -n 1 {$this->targetAddress}", $outcome, $status);
			if(0 === $status)
			{
				$pingTimeLine = end($outcome);
				preg_match_all("/ ([0-9]+)ms/", $pingTimeLine, $match);
				if(isset($match[1]))
				{
					$afterImage->setAiStatus        (__AFTERIMAGE_STATUS_NORMAL__);
					$afterImage->setAiElapsedTime   ($match[1][2]);
				}
				else
				{
					$afterImage->setAiStatus        (__AFTERIMAGE_STATUS_FAIL__);
					$afterImage->setAiElapsedTime   (0);
					$afterImage->setAiReturnCode    (array("Ping TimeOut"));
				}
			}
			else
			{
				$afterImage->setAiStatus        (__AFTERIMAGE_STATUS_FAIL__);
				$afterImage->setAiElapsedTime   (0);
				$afterImage->setAiReturnCode    (array("Ping Command Failed. RC", $status));
			}
		} elseif (strcasecmp(PHP_OS, 'Linux') === 0) {
			// Linux 服务器下
			$pingResult = exec("ping -c 1 {$this->targetAddress}", $outcome, $status);
			if(0 === $status)
			{
				$pingTimeLine = end($outcome);
				preg_match("/ ([0-9]+\.[0-9]+)\//", $pingTimeLine, $match);
				$afterImage->setAiStatus        (__AFTERIMAGE_STATUS_NORMAL__);
				$afterImage->setAiElapsedTime   (intval(round($match[1])));
			}
			else
			{
				$afterImage->setAiStatus        (false);
				$afterImage->setAiElapsedTime   (0);
			}
		}
		unset($status);
	}
}

