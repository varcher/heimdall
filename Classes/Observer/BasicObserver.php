<?php
/**
 * Created by PhpStorm.
 * User: varcher
 * Date: 2017/8/13
 * Time: 下午11:14
 */

class BasicObserver extends Observer
{
    protected $galaxyName   =   "";
    protected $universeName =   "";

    /**
     * @return string
     */
    public function getGalaxyName(): string
    {
        return $this->galaxyName;
    }

    /**
     * @return string
     */
    public function getUniverseName(): string
    {
        return $this->universeName;
    }


    public function __construct(array $inputObserverParams)
    {
        parent::__construct($inputObserverParams);
        $this->galaxyName   = $inputObserverParams['obGalaxyName'];
        $this->universeName = $inputObserverParams['obUniverseName'];
    }

    protected function doObserve(AfterImage $afterImage)
    {
        return;
    }

}