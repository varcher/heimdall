<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/8
 * Time: 13:56
 */

require_once "DNSObserver.php";
require_once "HTTPObserver.php";
require_once "PingObserver.php";
require_once "BasicObserver.php";

use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;

abstract class Observer
{
    protected $obNumber           =   -1;
    protected $obGalaxyNo         =   -1;
    protected $obUniverseNo       =   -1;
    protected $obType             =   -1;
    private   $obName             =   "";

    protected $alertType		=	0;    //TODO:变成数组，可选多个MESSENGER，Messenger为一个CLASS
    private   $obSlow			=	0;
    private   $obTimeOut		=	0;

    protected  $db		 		= 	'dbHeimdall';


    /**
	 * ObserverAbstract constructor.
	 * @param $inputObserverParams
	 * @internal param $inputObNumber
	 * @internal param $inputObPosition
	 * @internal param $inputObType
	 * @internal param $inputObType
	 * @internal param $inputTargetAddress
	 */
	function __construct(array $inputObserverParams)
	{
		$this->obNumber         =   $inputObserverParams['obNumber'];
		$this->obGalaxyNo       =   $inputObserverParams['obGalaxyNo'];
		$this->obUniverseNo     =   $inputObserverParams['obUniverseNo'];
		$this->obType           =   $inputObserverParams['obType'];
		$this->obName           =   $inputObserverParams['obName'];
		$this->alertType        =   $inputObserverParams['alertType'];
		$this->obSlow			=	$inputObserverParams['obSlow'];
		$this->obTimeOut		=	$inputObserverParams['obLost'];

		$this->db				=	Instance::ensure($this->db, Connection::className());    //todo: 后续拓展为统一使用一个基类，封装相同的功能
		return;
	}


	/**
	 * @return int
	 */
	public function getObNumber()
	{
		return $this->obNumber;
	}

	/**
	 * @return int
	 */
	public function getObUniverseNo()
	{
		return $this->obUniverseNo;
	}

	/**
	 * @return int
	 */
	public function getObType()
	{
		return $this->obType;
	}

	/**
	 * @return int
	 */

	/**
	 * @return string
	 */
	public function getObName()
	{
		return $this->obName;
	}



	/**
	 * @return int
	 */
	public function getAlertType()
	{
		return $this->alertType;
	}

	/**
	 * @return int
	 */
	public function getObGalaxyNo()
	{
		return $this->obGalaxyNo;
	}

	/**
	 * @return string
	 */
	public function getTargetAddress()
	{
		return $this->targetAddress;
	}

	function    observe()
	{
		$afterImage =   new AfterImage();
		$afterImage ->  setAiNumber    ($this->getObNumber());

		$this       ->  doObserve($afterImage);

		/*设置缓慢与超时状态*/
		if (__AFTERIMAGE_STATUS_NORMAL__ == $afterImage->getAiStatus()) {
			if ($afterImage->getAiElapsedTime() > $this->obTimeOut) {
				$afterImage->setAiStatus(__AFTERIMAGE_STATUS_TIMEOUT__);
			}
			elseif($afterImage->getAiElapsedTime() > $this->obSlow) {
				$afterImage->setAiStatus(__AFTERIMAGE_STATUS_SLOW__);
			}
		}
		return          $afterImage;
	}

	protected abstract function doObserve(AfterImage $afterImage);
}