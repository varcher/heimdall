<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/8
 * Time: 13:57
 */

class HttpObserver  extends Observer
{
	private $targetAddress	=	"";
	private $targetCookie	=	"";

	public function __construct($inputObserverParams)
	{
		parent::__construct($inputObserverParams);
		$command = $this->db->createCommand("SELECT obAddress, obCookie FROM observerHTTP WHERE observerNo =  {$this->obNumber}");
		if ($query = $command->queryOne()) {
			$this->targetAddress = $query['obAddress'];
			$this->targetCookie = $query['obCookie'];
		}
		else {
			throwException("NoSuchDNSAddress");
		}
	}

	protected function doObserve(AfterImage $afterImage)
	{
		// 1. 初始化
		$curlHandler = curl_init();
		// 2. 设置选项，包括URL
		curl_setopt($curlHandler, CURLOPT_URL,            $this->targetAddress);
		curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curlHandler, CURLOPT_HEADER,         true);
		curl_setopt($curlHandler, CURLOPT_TIMEOUT, 	       __PARAM_HTTP_TIMEOUT__);
		curl_setopt($curlHandler, CURLOPT_COOKIE, 			$this->targetCookie);
		curl_setopt($curlHandler, CURLOPT_FOLLOWLOCATION, true);

		// 3. 执行并获取HTML文档内容
		$output = curl_exec($curlHandler);
//			echo $output;
		if($output === FALSE ){
			$afterImage->setAiStatus        (__AFTERIMAGE_STATUS_FAIL__);
			$afterImage->setAiReturnCode    (array("CURL", curl_errno($curlHandler)));
		}
		else
		{
			$curlInfo = curl_getinfo($curlHandler);
            if (200 == $curlInfo['http_code']) {
                $afterImage->setAiStatus(__AFTERIMAGE_STATUS_NORMAL__);
            }
            else
            {
                $afterImage->setAiStatus(__AFTERIMAGE_STATUS_FAIL__);
                $afterImage->setAiReturnCode    (array("HTTP", $curlInfo['http_code']));
            }

			$afterImage->setAiElapsedTime   ($curlInfo['total_time'] * 1000);
		}
		// 4. 释放curl句柄
		curl_close($curlHandler);

	}
}