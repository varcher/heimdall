<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/8
 * Time: 13:57
 */

class DNSObserver extends Observer
{
	private $targetAddress	=	"";
	private $resultAddress	=	"";

	public function __construct($inputObserverParams)
	{
		parent::__construct($inputObserverParams);
		$command = $this->db->createCommand("SELECT obAddress, obResult FROM observerDNS WHERE observerNo =  {$this->obNumber}");
		if ($query = $command->queryOne()) {
			$this->targetAddress = $query['obAddress'];
			$this->resultAddress = $query['obResult'];
		}
		else {
			throwException("NoSuchDNSAddress");
		}
	}
	protected function doObserve(AfterImage $afterImage)
	{
		$startMicroTime = microtime(true);
		$returnAddress  =   gethostbyname($this->targetAddress);
		$endMicroTime = microtime(true);
		if($returnAddress   !=  $this->resultAddress)
		{
			$afterImage->setAiStatus        (__AFTERIMAGE_STATUS_FAIL__);
			$afterImage->setAiReturnCode    (['DNS ERROR', "SHOULD BE {$this->resultAddress}", "NOW IS {$returnAddress}"]);
			$afterImage->setAiElapsedTime   (round($endMicroTime - $startMicroTime, 3) * 1000);
		}
		else
		{
			$afterImage->setAiStatus(__AFTERIMAGE_STATUS_NORMAL__);
			$afterImage->setAiElapsedTime(round($endMicroTime - $startMicroTime, 3) * 1000);
		}
	}
}