<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/8
 * Time: 18:03
 */

require_once(__DIR__ . "/../Historiographer/Historiographer.php");

class HistoriographerFactory
{

	public static function createHistoriographer(int $inputHgType, string $inputHgToken)
	{
		switch ($inputHgType) {
			case __HISTORIOGRAPHER_TYPE_DB__:
				$hg 						=   new HistoriographerDB($inputHgToken);
				break;
            case __HISTORIOGRAPHER_TYPE_DB_BATCH__:
                $hg 						=   new HistoriographerDBBatch($inputHgToken);
                break;
			default:throw (HistorigrapherTypeInvalidException);
		}
		return $hg;

	}
}