<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/10
 * Time: 17:32
 */

use yii\db\Connection;
use yii\db\Command;
use yii\db\Query;
use yii\di\Instance;


class Courier
{
	private $db = "dbHeimdall";

	public function __construct()
	{
		$this->db				=	Instance::ensure($this->db, Connection::className());    //todo: 后续拓展为统一使用一个基类，封装相同的功能
	}

	public function process(Verdict $verdict)
	{
	    if($verdict->getStatus() == __ARBITER_STATUS_NORMAL__)
        {
            return;
        }
        else
        {
            $this->send($verdict);
        }
	}

	protected function send(Verdict $verdict)
	{
		$message    =   "";
		$currTime   =   time();
		$lastTime	=	0;
		$newMessage	=	false;
        switch ($verdict->getStatus()) {
            case __ARBITER_STATUS_WARNING__:
                if ($currTime - __MESSENGER_WECHAT_MIN_INTERVAL__ > max($verdict->getLastSendWarn() ,$verdict->getLastSendAlert()))
                {
                	$lastTime	=	max($verdict->getLastSendWarn() ,$verdict->getLastSendAlert());
                    $verdict->setLastSendWarn($currTime);
                }
                else
                {
                    return;
                }
                break;
            case __ARBITER_STATUS_FATAL__:
                if ($currTime - __MESSENGER_WECHAT_MIN_INTERVAL__ > $verdict->getLastSendAlert()) {

					$lastTime	=	$verdict->getLastSendAlert();
                    $verdict->setLastSendAlert($currTime);
                }
                else
                {
                    return;
                }
                break;
        }

        switch ($verdict->getStatus()) {
            case  __ARBITER_STATUS_WARNING__:
                $message = ">>预警<<";
                break;
            case __ARBITER_STATUS_FATAL__:
                $message = "!!异常!!";
                break;
        }

        $observer = $verdict->getObserver();
		$message .= "
位置：{$observer->getUniverseName()}（{$observer->getObUniverseNo()}）
应用：{$observer->getGalaxyName()}（{$observer->getObGalaxyNo()}）
监控点：{$observer->getObName()}（{$observer->getObNumber()}）
时间：".date("Y-m-d H:i:s", time())."\n";

        foreach ($verdict->getAfterImageList() as  $afterImage) {
			if ($afterImage->getAiTime() > $lastTime) {
				switch ($afterImage->getAiStatus()) {
					case __AFTERIMAGE_STATUS_NORMAL__:
						break;
					case __AFTERIMAGE_STATUS_FAIL__:
						$message    .=  "【异常】\n".date("Y-m-d H:i:s", $afterImage->getAiTime())."\n" . join(":", $afterImage->getAiReturnCode())."\n";
						break;
					case __AFTERIMAGE_STATUS_SLOW__:
						$message    .=  "（缓慢）\n".date("Y-m-d H:i:s", $afterImage->getAiTime())."\n延时 = {$afterImage->getAiElapsedTime()}\n";
						break;
					case __AFTERIMAGE_STATUS_TIMEOUT__:
						$message    .=  "【超时】\n".date("Y-m-d H:i:s", $afterImage->getAiTime())."\n延时 = {$afterImage->getAiElapsedTime()}\n";
						break;
				}
				$newMessage	=	true;
			}
        }
		if ($newMessage) {
			sendWeChatMessage($message);    //TODO:后续改成按类型发送
		}
	}
}