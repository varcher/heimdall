<?php
/**
 * Created by PhpStorm.
 * User: varcher
 * Date: 2017/8/8
 * Time: 下午11:57
 */

class Heimdall
{
    private static $observerList        =   array();
    private static $basicObserverList   =   array();

    public static function addObserver(Observer $observer)
    {
        if(!isset(self::$observerList[$observer->getObNumber()])) {
            self::$observerList[$observer->getObNumber()] = $observer;
        }
    }

//    public static function showAllObserver()
//    {
//        var_dump(self::$observerList);
//    }

    public static function getObserver($inObNumber) :Observer
    {
        if (!isset(self::$observerList[$inObNumber]))
        {
            self::addObserver(ObserverFactory::createObserver($inObNumber));
        }
		return self::$observerList[$inObNumber];
    }

	public static function getBasicObserver($inObNumber) : BasicObserver
	{
		if (isset(self::$basicObserverList[$inObNumber]))
		{
			return self::$basicObserverList[$inObNumber];
		}
		else
		{
			$inObserver =   ObserverFactory::createObserver($inObNumber, "Basic");
			self::$basicObserverList[$inObNumber]	=	$inObserver;
			return $inObserver;
		}
	}
}