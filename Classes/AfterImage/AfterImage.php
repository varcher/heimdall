<?php
/**
 * Created by PhpStorm.
 * User: varcher
 * Date: 2017/8/4
 * Time: 下午11:19
 */

class AfterImage
{

    protected $aiNumber         =   -1;

    protected $aiTime           =   0;
    protected $aiStatus         =   0;
    protected $aiElapsedTime    =   0;
    protected $aiReturnCode     =   array();


    public function __construct()
    {
        $this->setAiTime(time());
    }

    /**
     * @return int
     */
    public function getAiNumber(): int
    {
        return $this->aiNumber;
    }

	/**
	 * @param int $aiTime
	 */
	public function setAiTime(int $aiTime)
	{
		$this->aiTime = $aiTime;
	}

    /**
     * @param int $aiNumber
     */
    public function setAiNumber(int $aiNumber)
    {
        $this->aiNumber = $aiNumber;
    }

    /**
     * @return int
     */
    public function getAiTime(): int
    {
        return $this->aiTime;
    }

    /**
     * @return int
     */
    public function getAiStatus(): int
    {
        return $this->aiStatus;
    }

    /**
     * @param int $aiStatus
     */
    public function setAiStatus(int $aiStatus)
    {
        $this->aiStatus = $aiStatus;
    }

    /**
     * @return int
     */
    public function getAiElapsedTime(): int
    {
        return $this->aiElapsedTime;
    }

    /**
     * @param int $aiElapsedTime
     */
    public function setAiElapsedTime(int $aiElapsedTime)
    {
        $this->aiElapsedTime = $aiElapsedTime;
    }

    /**
     * @return array
     */
    public function getAiReturnCode(): array
    {
        return $this->aiReturnCode;
    }

    /**
     * @param array $aiReturnCode
     */
    public function setAiReturnCode(array $aiReturnCode)
    {
        $this->aiReturnCode = $aiReturnCode;
    }


}

