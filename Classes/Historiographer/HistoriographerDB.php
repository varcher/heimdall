<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/8
 * Time: 17:08
 */
use yii\db\Connection;
use yii\db\Command;
use yii\db\Query;
use yii\di\Instance;

class HistoriographerDB extends Historiographer
{
	protected $db = "";

	public function __construct(string $inDb = "dbHeimdallLog")
	{
		$this->db				=	Instance::ensure($inDb, Connection::className());
	}

	public function record(AfterImage $afterImage)
	{
        try {
            $command = $this->db->createCommand()->insert('afterimageLog', $this->buildArray($afterImage));
            $command->execute();
        } catch (Exception $exception) {
            Yii::error("HistoriographerDB::Insert afterimageLog Failed, reason = {$exception->getMessage()}");
            Yii::error("Array is ".join("||", $this->buildArray($afterImage)));
        }
	}
}

