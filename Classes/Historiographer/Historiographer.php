<?php
/**
 * Created by PhpStorm.
 * User: varcher
 * Date: 2017/8/5
 * Time: 上午12:16
 */

use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;

require_once "HistoriographerDB.php";
require_once "HistoriographerDBBatch.php";

abstract class Historiographer
{
	abstract public function record(AfterImage $afterImage);

	protected function buildArray(AfterImage $afterImage)
	{
		$observer   =   Heimdall::getObserver($afterImage->getAiNumber());
		$arr	=	[
			"aiNo"			=>	$afterImage->getAiNumber(),
			"aiTime"		=>	date("Y-m-d H:i:s", $afterImage->getAiTime()),
			"aiName"		=>	$observer->getObName(),
			"aiGalaxyNo"	=>	$observer->getObGalaxyNo(),
			"aiUniverseNo"	=>	$observer->getObUniverseNo(),
			"aiType"		=>	$observer->getObType(),
			"aiStatus"		=>	$afterImage->getAiStatus(),
			"aiElapsedTime"	=>	$afterImage->getAiElapsedTime(),
			"aiReturnCode"	=>	join("||", $afterImage->getAiReturnCode()),
		];

		return $arr;
	}
}