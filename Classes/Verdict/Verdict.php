<?php
/**
 * Created by PhpStorm.
 * User: varcher
 * Date: 2017/8/20
 * Time: 下午10:52
 */

class Verdict
{
    protected $status           =   __ARBITER_STATUS_NORMAL__;
    protected $aiNo             =   -1;
    protected $observer         =   null;
    protected $lastSendWarn     =   0;
    protected $lastSendAlert    =   0;
    protected $afterImageList   =   array();


    public function __construct(int $aiNo)
    {
        $this->aiNo     =   $aiNo;
        $this->observer = Heimdall::getBasicObserver($aiNo);
    }

    /**
     * @return int
     */
    public function getAiNo(): int
    {
        return $this->aiNo;
    }

    /**
     * @return BasicObserver|null
     */
    public function getObserver()
    {
        return $this->observer;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getLastSendWarn(): int
    {
        return $this->lastSendWarn;
    }

    /**
     * @param int $lastSendWarn
     */
    public function setLastSendWarn(int $lastSendWarn)
    {
        $this->lastSendWarn = $lastSendWarn;
    }

    /**
     * @return int
     */
    public function getLastSendAlert(): int
    {
        return $this->lastSendAlert;
    }

    /**
     * @param int $lastSendAlert
     */
    public function setLastSendAlert(int $lastSendAlert)
    {
        $this->lastSendAlert = $lastSendAlert;
    }

    /**
     * @return array
     */
    public function getAfterImageList(): array
    {
        return $this->afterImageList;
    }

    /**
     * @param array $afterImageList
     */
    public function setAfterImageList(array $afterImageList)
    {
        $this->afterImageList = $afterImageList;
    }


}