<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/10
 * Time: 10:03
 */

use yii\db\Connection;
use yii\db\Command;
use yii\db\Query;
use yii\di\Instance;

abstract class Carrier
{
	protected  $db		 		= 	'dbHeimdall';

//	protected $observerList	=	array();		//todo:先做成获取全部，后续再按Historiographer加载
	protected $carrier 		= 	array();
	private	$aiList			=	array();
	protected $lastPutTime		=	0;

	public function __construct()
	{
	}

	public abstract function fetch();

	public function get(int $observerNo)
	{
		if ($this->isHave($observerNo)) {
//			var_dump($this->carrier["{$observerNo}"]);
			return $this->carrier[$observerNo];
		}
		else {
			return null;
		}
	}

	public function getLast(int $observerNo): AfterImage
	{
		if ($this->isHave($observerNo)) {
			return $this->get($observerNo)[sizeof($this->get($observerNo)) - 1];
		}
		else {
			return null;
		}
	}

	protected function getLastTime(int $observerNo) : int
	{
		if($this->isHave($observerNo))
		{
			return  $this->getLast($observerNo)->getAiTime();
		}
		else {
			return 0;
		}
	}

	protected function isHave(int $observerNo) : bool
	{
		return isset($this->carrier[$observerNo]);
	}

	protected function put(AfterImage $afterImage)
	{
		$observerNo			= 	$afterImage->getAiNumber();
		$this->lastPutTime	=	$afterImage->getAiTime();
		if(isset($this->carrier[$observerNo]))
		{
			array_push($this->carrier[$observerNo], $afterImage);
			if(sizeof($this->carrier[$observerNo]) > __CARRIER_MAX_LOAD__)
			{
				array_shift($this->carrier[$observerNo]);
			}
		}
		else {
			$this->carrier[$observerNo][0]	=	$afterImage;
			array_push($this->aiList, $observerNo);
		}
	}

	public function getAi(int $observerNo, $seqNo) : AfterImage
	{
		return $this->carrier[$observerNo][$seqNo];
	}

	public function getAiList()
	{
		return $this->aiList;
	}

	public function testOutput()
	{
		print_r($this->carrier);
	}

}