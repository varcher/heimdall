<?php
/**
 * Created by PhpStorm.
 * User: varcher
 * Date: 2017/8/20
 * Time: 下午11:46
 */

class WatchMen
{
    private $carrier        =   null;
    private $courier        =   null;
    private $verdictList    =   null;

    public function __construct(int $hgNo = -1)
    {
        //TODO: 后续做成按hgNo加载的
        $this->carrier      =   new CarrierDB();
        $this->courier      =   new Courier();
        $this->verdictList  =   new VerdictList();
    }

    public function watch()
    {
        $this->carrier->fetch();
        foreach ($this->carrier->getAiList() as $aiNo) {
            $verdict        =   $this->verdictList->getVerdict($aiNo);
            $verdict        ->  setAfterImageList($this->carrier->get($aiNo));
            Arbiter         ::  arbitrate($verdict);
            $this->courier  ->  process($verdict);
        }
    }
}

class VerdictList
{
    private $verdictList;

    public function getVerdict($aiNo) : Verdict
    {
        if (!isset($this->verdictList[$aiNo])) {
            $this->verdictList[$aiNo]   =   new Verdict($aiNo);
			$this->verdictList[$aiNo]->setAiNo($aiNo);
        }
        return $this->verdictList[$aiNo];
    }
}