<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/7
 * Time: 17:37
 */

use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;

class Galaxy
{

	private $galaxyNo				=	null;
	private $galaxyName				=	null;
	private $galaxyBelongUniverseNo	=	null;
	private $observers 				= 	array();
	private $historiographers		=	array();

	private  $db		 	= 	'dbHeimdall';


	public function create(array $inGalaxy, array $inHistoriographers)
	{
		/*
		 * 建立数据库示例
		 */
		$this->db = Instance::ensure($this->db, Connection::className());    //todo: 后续拓展为统一使用一个基类，封装相同的功能
		$this->galaxyNo = $inGalaxy['galaxyNo'];
		$this->galaxyName = $inGalaxy['galaxyName'];
		$this->galaxyBelongUniverseNo = $inGalaxy['universeNo'];
		$this->historiographers = $inHistoriographers;

		$this->createObservers();
	}

    /** 建立Observer集合 */
	public function createObservers()
	{
		$command = $this->db->createCommand("SELECT observerNo FROM observer WHERE galaxyNo =  {$this->galaxyNo}");
		$query = $command->queryAll();
		foreach ($query as $inObserver) {
			$observer = ObserverFactory::createObserver($inObserver['observerNo']);
			array_push($this->observers, $observer);
		}
	}

	public function stare()
	{
		foreach ($this->observers as $observer) {
			$afterImage = $observer->observe();
			foreach ($this->historiographers as $historiographer) {
				$historiographer->record($afterImage);
			}
		}

		//处理AI
	}
}
