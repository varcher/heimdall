<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/8
 * Time: 14:00
 */

require_once(__DIR__ . "/../Observer/Observer.php");

use yii\db\Connection;
use yii\db\Command;
use yii\db\Query;
use yii\di\Instance;


class   ObserverFactory
{

	/**
	 * Observer constructor.
	 * @param $inputObserverParams
	 * @return DNSObserver|HttpObserver|PingObserver
	 * @throws
	 * @internal param $observer
	 */

    private static $dbName      =   "dbHeimdall";
    private static $dbHandler   =   null;

	public static function createObserver($inputObserverNo, $type   =   "Normal"): Observer
	{
        if (!self::$dbHandler) {
            self::$dbHandler    =   Instance::ensure(self::$dbName, Connection::className());
        }

        switch ($type) {
            case "Normal":
                return self::createNoemalObserver($inputObserverNo);
                break;
            case "Basic":
                return self::createBasicObserver($inputObserverNo);
                break;
            default:
                throw(ObserverTypeInvalidException);
        }



	}

    private static function createNoemalObserver($inputObserverNo)
    {
        $obParams = self::getObserverInfo($inputObserverNo);

        switch ($obParams["obType"]) {
            case __OBSERVER_TYPE_PING__:
                $observer 						=   new PingObserver($obParams);
                break;
            case __OBSERVER_TYPE_HTTP__:
                $observer 						=   new HttpObserver($obParams);
                break;
            case __OBSERVER_TYPE_DNS__ :
                $observer 						=   new DNSObserver ($obParams);
                break;
            default:throw (ObserverTypeInvalidException);
        }
        Heimdall::addObserver($observer);
        return $observer;
    }

    private static function createBasicObserver(int $inputObserverNo): Observer
    {
        $obParams = self::getObserverInfo($inputObserverNo);
        $observer = new BasicObserver($obParams);
        return $observer;
    }

    private static function getObserverInfo(int $inputObserverNo): array
    {

        $command = self::$dbHandler->createCommand("SELECT * FROM observer WHERE observerNo =  {$inputObserverNo}");
        $query = $command->queryOne();
        $obParams = [
            'obNumber'	 	=>	$query['observerNo'],
            'obGalaxyNo'	=> 	$query['galaxyNo'],
            'obName' 		=> 	$query['observerName'],
            'obType' 		=> 	$query['observerType'],
            'alertType' 	=> 	$query['observerAlertType'],
            'obSlow' 		=> 	$query['observerSlowThreshold'],
            'obLost' 		=> 	$query['observerLostThreshold']
        ];

        $command = self::$dbHandler->createCommand("SELECT universeNo, universeName, galaxyName FROM observerdesc WHERE observerNo =  {$inputObserverNo}");
        $query = $command->queryOne();
        $obParams['obUniverseNo']   = $query['universeNo'];
        $obParams['obUniverseName'] = $query['universeName'];
        $obParams['obGalaxyName']   = $query['galaxyName'];

        return $obParams;
    }
}

