<?php
/**
 * Created by PhpStorm.
 * User: varcher
 * Date: 2017/12/30
 * Time: 下午10:01
 */

require_once "GiallarWeOA.php";

use yii\db\Connection;
use yii\db\Query;
use yii\di\Instance;

abstract class Giallar
{

    function __construct()
    {

    }

    abstract function send(string $sendMessage, string $sendSubject, string $sendReceiver);

}


