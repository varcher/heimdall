<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/7/19
 * Time: 22:16
 */

require_once "Classes/AfterImage.php";
include "ObserverFactoryClass.php";
include "ObserverList.php";
include "constant.php";

include "log4php/Logger.php";

date_default_timezone_set('PRC');

Logger::configure(dirname(__FILE__).'/logger.xml');
$logger = Logger::getLogger('myLogger');


$observerFactory    =   new ObserverFactory();

echo "\"They shall not pass.  --- Heimdall\"<br><br><br>";

foreach($observerList as $inObserverList)
{
	$myObserver    =   $observerFactory->createObserver($inObserverList);
	$myObserver->observe();
	simpleOutputSite($myObserver);
//	echo "=====================<br>\n";
}

echo "最后更新时间：".date("Y-m-d.H:i:s", time());

exit(0);


/**
 * @param Observer $inObserver
 */
function simpleOutputSite(Observer $inObserver)
{
	$normallogger   =   Logger::getLogger('myLogger');
	$eventLogger    =   Logger::getLogger('event');
	if(true == $inObserver->getLastObStatus())
	{
		echo "站点：{$inObserver->getObName()}　　状态：<font color=green>正常</font>，响应时间：{$inObserver->getLastObElapsedTime()}<br>";
		$normallogger->info("站点：{$inObserver->getObName()}　　状态：正常");
	}
	else
	{
		echo "站点：{$inObserver->getObName()}　　状态：<font color=red><b>异常！</b></font><br>";
		$eventLogger->warn("站点：{$inObserver->getObName()}　　状态：异常");

		switch($inObserver->getAlertType()) {
			case __MESSENGER_TYPE_OFF__:
				break;
			case __MESSENGER_TYPE_WECHAT__:
				sendWeChatMessage($inObserver->getObName());
				break;
			default:
				break;
		}
	}

}

function sendWeChatMessage($inputObName)
{

	$message = preg_replace("/DDDD/", "【{$inputObName}】好像坏了！赶紧看看吧！！".date("Y-m-d.H:i:s", time()), __MESSENGER_WECHAT_URL__);
//	echo $message;
	$curlHandler = curl_init();
	curl_setopt($curlHandler, CURLOPT_URL, $message );
	curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curlHandler, CURLOPT_HEADER,         1);

	//$output = curl_exec($curlHandler);
	//echo $output;
	curl_close($curlHandler);

}
?>