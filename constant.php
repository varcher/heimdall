<?php
/**
 * Created by PhpStorm.
 * User: Vagrant Archer
 * Date: 2017/8/4
 * Time: 17:17
 */

require_once "WechatConstant.php";

define("__HEIMDALL_INTERVAL__", 5); //时间间隔

define("__OBSERVER_POSITION_TEST__", 0);
define("__OBSERVER_POSITION_INNER__", 1);
define("__OBSERVER_POSITION_OUTTER__", 2);

define("__OBSERVER_TYPE_PING__", 0);
define("__OBSERVER_TYPE_HTTP__", 1);
define("__OBSERVER_TYPE_SITE_ACCESS__", 2);	//交互式访问，需要做登陆和退出
define("__OBSERVER_TYPE_DNS__", 3);


define("__MESSENGER_TYPE_OFF__", 0);
define("__MESSENGER_TYPE_WECHAT__", 1);

define("__PARAM_HTTP_TIMEOUT__", 60);   //
define("__PARAM_HTTP_TIMELONG__", 10);

define("__MESSENGER_WECHAT_MIN_INTERVAL__", 120);    //微信通知最小间隔
define("__MESSENGER_WECHAT_MAX_MESSAGES__", 20);   //微信通知单位时间最大条数
define("__MESSENGER_WECHAT_MAX_MESSAGES_TIME__", 3600);    //微信通知单位时间间隔


define("__AFTERIMAGE_STATUS_FAIL__", 0);
define("__AFTERIMAGE_STATUS_NORMAL__", 1);
define("__AFTERIMAGE_STATUS_SLOW__", 2);
define("__AFTERIMAGE_STATUS_TIMEOUT__", 3);

define("__HISTORIOGRAPHER_TYPE_LOG__", 0);
define("__HISTORIOGRAPHER_TYPE_DB__", 1);
define("__HISTORIOGRAPHER_TYPE_DB_BATCH__", 2);
define("__HISTORIOGRAPHER_DBBATCH_FLUSH_INTERVAL__", 100); //DB BATCH模式下多少个IMAGE插入一次

define("__DIR_PIDFILE_PATH__", ""); //TODO:MUST SET FOR ENVIRONMENT
define("__DIR_LOGFILE_PATH__", ""); //TODO:MUST SET FOR ENVIRONMENT

define("__CARRIER_MAX_LOAD__", 50);
define("__CARRIER_MAX_INTERVAL__", 3600);	//超过多少间隔的事件就不发送了

define("__ARBITER_COUNT_NUMBER__", 10);
define("__ARBITER_RIGHT_NORMAL__", 0);
define("__ARBITER_RIGHT_SLOW__", 5);
define("__ARBITER_RIGHT_TIMEOUT__", 10);
define("__ARBITER_RIGHT_FAIL__", 10);
define("__ATBITER_RIGHT_THRESHOLD_WARNING__", 16);
define("__ATBITER_RIGHT_THRESHOLD_FATAL__", 30);

define("__ARBITER_STATUS_NORMAL__", 0);
define("__ARBITER_STATUS_WARNING__", 1);
define("__ARBITER_STATUS_FATAL__", 2);

define("__WECHAT_ONE_HOUR_SECONDS__", 3600);

define("__CONTROLLER_LONGRUNNING_ACTION__", ["start", "watchmen"]);