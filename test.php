<?php

$lineTrafficInfo = array(
    'name' => "TEST",
    "up"   => 1000,
    "down" => 500,
    "speed"=> 1500,
);

$speedIcon = "[----------]";
$number = ceil( max($lineTrafficInfo['up'], $lineTrafficInfo['down']) * 10 / $lineTrafficInfo['speed']);
echo $number;
$speedIcon = preg_replace("/-/", "I", $speedIcon, $number);
$output ="{$lineTrafficInfo['name']}:{$speedIcon}\n";
$output .= "{$lineTrafficInfo['up']}/{$lineTrafficInfo['down']}/{$lineTrafficInfo['speed']}(U/D/Max)";
echo $output;

?>